from django.shortcuts import render
from rest_framework import viewsets
from .models import Card
from .serializers import CardSerializer, UserSerializer
from django.contrib.auth.models import User
from rest_framework import permissions


class CardViewSet(viewsets.ModelViewSet):
	"""ViewSet for cards"""
	queryset = Card.objects.all()
	serializer_class = CardSerializer
	permission_classes = (permissions.IsAuthenticated,)


class CardByUserViewSet(viewsets.ModelViewSet):
	"""ViewSet for cards by user"""
	serializer_class = CardSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		return self.request.user.cards.all()

		
	
class UserViewSet(viewsets.ModelViewSet):
	"""ViewSet for users"""
	queryset = User.objects.all()
	serializer_class = UserSerializer
	permission_classes = (permissions.IsAuthenticated,)