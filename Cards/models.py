from django.db import models
from django.contrib.auth.models import User


class Card(models.Model):
	"""Model of card"""
	front = models.CharField(max_length=200)
	back = models.CharField(max_length=200)
	person = models.ForeignKey('auth.User', related_name='cards')

	def __str__(self):
		return front