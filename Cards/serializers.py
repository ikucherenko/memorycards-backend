from rest_framework import serializers
from .models import Card
from django.contrib.auth.models import User


class CardSerializer(serializers.HyperlinkedModelSerializer):
	"""Serializer for card model"""
	person = serializers.PrimaryKeyRelatedField(queryset=User.objects.all()
	)
	class Meta:
		model = Card
		fields = ('front', 'back', 'person')


class UserSerializer(serializers.ModelSerializer):
	"""Serializer for user model"""

	cards = serializers.PrimaryKeyRelatedField(
		many=True, queryset=Card.objects.all() # Additional field for access to list of cards for user
	)

	class Meta:
		model = User
		fields = ('username', 'email', 'is_staff', 'cards')
