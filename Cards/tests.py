from django.test import TestCase, Client
from .models import Card
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework_jwt import utils
from rest_framework import status

class CardAPITests(TestCase):

	def setUp(self):
		"""Create new test user"""
		self.csrf_client = APIClient(enforce_csrf_checks=True)
		self.username = 'user'
		self.email = 'user@example.com'
		self.user = User.objects.create_user(self.username, self.email)
		self.payload = utils.jwt_payload_handler(self.user)
		self.token = utils.jwt_encode_handler(self.payload)
		self.id = self.user.id
		self.auth = 'JWT {0}'.format(self.token)

	def test_cards_by_user_api_without_login(self):
		"""Anonymous user can`t see self models"""
		client = APIClient()
		response = client.get('/user-cards/')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_cards_api_without_login(self):
		"""Anonymous user can`t see any models"""
		client = APIClient()
		response = client.get('/cards/')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_user_api_without_login(self):
		"""Anonymous user can`t see list of users"""
		client = APIClient()
		response = client.get('/users/')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_post_form_passing_jwt_auth(self):
		"""User can login and post card"""
		response = self.csrf_client.post(
			'/cards/', 
			{"front": "дом","back": "home","person": self.id}, 
			HTTP_AUTHORIZATION=self.auth
			)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_get_from_passing_jwt_auth(self):
		"""User can login and get list of his cards"""
		data = {"front": "house","back": "home","person": self.id}
		self.csrf_client.post(
			'/cards/', data
			, 
			HTTP_AUTHORIZATION=self.auth
		)
		response = self.csrf_client.get('/user-cards/', HTTP_AUTHORIZATION=self.auth)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.content, b'[{"front":"house","back":"home","person":1}]')
